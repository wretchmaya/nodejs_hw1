const http = require('http');
const fs = require('fs');
const url = require('url');
const dir = './file';
const logsJSON = './logs.json';

const getPreviousLogsJSON = () => {
	if (!fs.existsSync(logsJSON)) {
		const template = {
			logs: [],
		};
		fs.writeFileSync(logsJSON, JSON.stringify(template), err => {
			err ? console.log(err) : null;
		});
	}
	let data = fs.readFileSync(logsJSON, 'utf8');
	return data ? JSON.parse(data).logs : [];
};
const setCurrentLogsJSON = logs => {
	return fs.writeFile(logsJSON, JSON.stringify(logs), err =>
		err ? console.log(err) : null
	);
};
const createNewLogs = (currLogs, filename) => {
	const logsUnit = {
		message: `New file with name '${filename}' saved`,
		time: new Date().getTime(),
	};
	return [...currLogs, logsUnit];
};
module.exports = () => {
	http
		.createServer((request, response) => {
			const data = {
				logs: [...getPreviousLogsJSON()],
			};
			const { pathname, query } = url.parse(request.url, true);
			if (request.method === 'POST') {
				if (!query.content || !query.filename) {
					response.writeHead(400, { 'Context-type': 'text/html' });
					response.end('Incorrect parameters.');
				} else {
					if (!fs.existsSync(dir)) {
						fs.mkdirSync(dir);
					}
					fs.writeFile(`./file/${query.filename}`, query.content, 'utf8', err =>
						err ? console.log(err) : null
					);
					data.logs = createNewLogs(data.logs, query.filename);
					setCurrentLogsJSON(data);
					response.writeHead(200, { 'Context-type': 'text/html' });
					response.end();
				}
			} else if (request.method === 'GET') {
				if (pathname === '/logs') {
					response.writeHead(200, { 'Content-type': 'application/json' });
					response.end(JSON.stringify(data));
				} else if (pathname.includes('/file')) {
					let filename;
					pathname.includes(':')
						? (filename = pathname.split(':')[1])
						: (filename = pathname.split('/')[2]);

					fs.readFile(`./file/${filename}`, 'utf8', (err, content) => {
						if (err) {
							response.writeHead(400, { 'Content-type': 'text/html' });
							response.end(`no file with name "${filename}" was found.`);
							return;
						}
						response.writeHead(200, { 'Content-type': 'text/html' });
						response.end(content);
					});
				} else {
					response.writeHead(200, { 'Content-type': 'text/html' });
					response.end('Server is up.');
				}
			} else {
				response.writeHead(404, { 'Content-type': 'text/html' });
				response.end(`${request.method} is not supported.`);
			}
		})
		.listen(process.env.PORT || 8080);
};
